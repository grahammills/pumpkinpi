from gpiozero import MotionSensor
from neopixel import *
from multiprocessing import Process

import os
import pygame
import time
import random

# LED strip configuration:
LED_COUNT = 30  # Number of LED pixels.
LED_PIN = 18  # GPIO pin connected to the pixels (must support PWM!).
LED_FREQ_HZ = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA = 5  # DMA channel to use for generating signal (try 5)
LED_BRIGHTNESS = 200  # Set to 0 for darkest and 255 for brightest
LED_INVERT = False  # True to invert the signal (when using NPN transistor level shift)


def candle(strip, flicker):
    """Candle flicker effect"""
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, Color(80, 210, 1))
        strip.setBrightness(random.randint(64, 128))
        strip.show()
        time.sleep(random.random() / flicker)


def motionLoop(sound):
    """Motion sensor sound"""
    print("motion loop")

    # pygame.init()
    pir = MotionSensor(4)
    while True:
        pir.wait_for_motion()
        print("motion!")
        sound.play()
        time.sleep(5)
        sound.stop()


def lightLoop(strip):
    """Light strip loop"""
    while True:
        candle(strip, 5)

# Main program logic follows:
if __name__ == '__main__':
    # Setup mixer
    pygame.mixer.pre_init(44100, -16, 2, 2048)
    pygame.init()

    # Create NeoPixel object with appropriate configuration.
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS)
    # Initialize the library (must be called once before other functions).
    strip.begin()

    # Add sounds files
    try:
        pygame.mixer.music.load(os.path.join('assets', 'background.mp3'))
        sound = pygame.mixer.Sound(os.path.join('assets', 'motion.wav'))
    except:
        raise UserWarning, "could not load or play soundfiles :-("

    # Start music on loop
    pygame.mixer.music.play(-1)

    mixer = pygame.mixer

    p1 = Process(target=lightLoop, args=(strip,))
    #    p2 = Process(target=motionLoop, args=(sound,))
    p1.start()
    #    p2.start()
    #    p1.join()
    #    p2.join()

    # TODO can't get motion to work in a loop :-(
    # Wait for motion loop
    pir = MotionSensor(4)

try:
    while True:
        pir.wait_for_motion()
        sound.play()
        time.sleep(15)
        sound.stop()
except KeyboardInterrupt:
	for i in range(strip.numPixels()):
		strip.setPixelColor(i, Color(0, 0, 0))
	strip.show()
